# frozen_string_literal: true

require_relative '../lib/cli'

RSpec.describe CLI::IBGENames do
  context '.new' do
    it 'raise StandardError' do
      expect { CLI::IBGENames.new }.to raise_error(StandardError)
    end
  end

  context '.ranking' do
    context '.federative_unit_names' do
      it 'should print the general name ranking table to a respective Federative Unit' do
        arguments = %w[--ranking -u RJ]
        name_result_double = { male: [], female: [], general: [] }
        parser = CLI::OptionParser.new

        allow(parser).to receive(:parse).with(arguments)
                                        .and_return({
                                                      option: :ranking,
                                                      arguments: { federative_unit: 'RJ' }
                                                    })
        allow(Name).to receive(:ranking_by_uf).with(33).and_return(name_result_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('**GERAL**')
        ).to_stdout_from_any_process
      end

      it 'should print the male name ranking table to a respective Federative Unit' do
        arguments = %w[--ranking -u RJ]
        name_result_double = { male: [], female: [], general: [] }
        parser = CLI::OptionParser.new

        allow(parser).to receive(:parse).with(arguments)
                                        .and_return({
                                                      option: :ranking,
                                                      arguments: { federative_unit: 'RJ' }
                                                    })
        allow(Name).to receive(:ranking_by_uf).with(33).and_return(name_result_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('**MASCULINO**')
        ).to_stdout_from_any_process
      end

      it 'should print the female name ranking table to a respective Federative Unit' do
        arguments = %w[--ranking -u RJ]
        name_result_double = { male: [], female: [], general: [] }
        parser = CLI::OptionParser.new

        allow(parser).to receive(:parse).with(arguments)
                                        .and_return({
                                                      option: :ranking,
                                                      arguments: { federative_unit: 'RJ' }
                                                    })
        allow(Requester::Name).to receive(:ranking_by_uf).with(33).and_return(name_result_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('**FEMININO**')
        ).to_stdout_from_any_process
      end

      it 'an error message should appear when an invalid FederativeUnit is passed' do
        arguments = ['--ranking', '-u', 'ZZ']

        resp_double = double('faraday_response', status: 200, body: '[]')
        allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/ZZ')
                                       .and_return(resp_double)

        parser = CLI::OptionParser.new
        allow(parser).to receive(:parse).with(arguments)
                                        .and_return({
                                                      option: :ranking,
                                                      arguments: { federative_unit: 'ZZ' }
                                                    })

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('Um erro ocorreu: Unidade Federativa inválida.')
        ).to_stdout_from_any_process
      end

      it 'an error message should appear when an UF is not passed' do
        arguments = ['--ranking', '-u']

        parser = CLI::OptionParser.new
        allow(parser).to receive(:parse).with(arguments)
                                        .and_return({
                                                      option: :ranking,
                                                      arguments: { federative_unit: '' }
                                                    })

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('Um erro ocorreu: Parece que você esqueceu de passar uma UF.')
        ).to_stdout_from_any_process
      end
    end

    context '.city_names' do
      it 'should print the general name ranking table to a respective City' do
        arguments = ['--ranking', '-c', 'Campos dos Goytacazes']
        name_result_double = { male: [], female: [], general: [] }
        parser = CLI::OptionParser.new

        allow(parser).to receive(:parse).with(arguments)
                                        .and_return({
                                                      option: :ranking,
                                                      arguments: { city: 'Campos dos Goytacazes' }
                                                    })
        allow(Name).to receive(:ranking_by_city).with(330_100_9).and_return(name_result_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('**GERAL**')
        ).to_stdout_from_any_process
      end

      it 'should print the male name ranking table to a respective City' do
        arguments = ['--ranking', '-c', 'Campos dos Goytacazes']
        name_result_double = { male: [], female: [], general: [] }
        parser = CLI::OptionParser.new

        allow(parser).to receive(:parse).with(arguments)
                                        .and_return({
                                                      option: :ranking,
                                                      arguments: { city: 'Campos dos Goytacazes' }
                                                    })
        allow(Name).to receive(:ranking_by_city).with(330_100_9).and_return(name_result_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('**MASCULINO**')
        ).to_stdout_from_any_process
      end

      it 'should print the female name ranking table to a respective City' do
        arguments = ['--ranking', '-c', 'Campos dos Goytacazes']
        name_result_double = { male: [], female: [], general: [] }
        parser = CLI::OptionParser.new

        allow(parser).to receive(:parse).with(arguments)
                                        .and_return({
                                                      option: :ranking,
                                                      arguments: { city: 'Campos dos Goytacazes' }
                                                    })
        allow(Requester::Name).to receive(:ranking_by_city).with(330_100_9).and_return(name_result_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('**FEMININO**')
        ).to_stdout_from_any_process
      end

      it 'an error message should appear when an invalid City is passed' do
        arguments = ['--ranking', '-c', 'Z City']

        resp_double = double('faraday_response', status: 200, body: '[]')
        allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/municipios/Z-City')
                                       .and_return(resp_double)

        parser = CLI::OptionParser.new
        allow(parser).to receive(:parse).with(arguments)
                                        .and_return({
                                                      option: :ranking,
                                                      arguments: { city: 'Z City' }
                                                    })

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('Um erro ocorreu: Cidade inválida.')
        ).to_stdout_from_any_process
      end

      it 'an error message should appear when a City is not passed' do
        arguments = ['--ranking', '-c']

        resp_double = double('faraday_response', status: 200, body: '[]')
        allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/municipios/Z-City')
                                       .and_return(resp_double)

        parser = CLI::OptionParser.new
        allow(parser).to receive(:parse).with(arguments)
                                        .and_return({
                                                      option: :ranking,
                                                      arguments: { city: '' }
                                                    })

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('Um erro ocorreu: Parece que você esqueceu de passar o nome de uma cidade.')
        ).to_stdout_from_any_process
      end
    end
  end

  context '.frequency' do
    context '.names_frequency' do
      it 'should print the headings of name frequency table' do
        arguments = ['--frequencia', '-n', 'Matheus, João, Maria']
        frequencies = {
          '1930' => 1, '1930-1940' => 2, '1940-1950' => 3, '1950-1960' => 4, '1960-1970' => 5,
          '1970-1980' => 6, '1980-1990' => 7, '1990-2000' => 8, '2000-2010' => 9
        }
        matheus_frequency_double = { name: 'Matheus', frequencies: frequencies }
        joao_frequency_double = { name: 'Joao', frequencies: frequencies.transform_values {|v| v + 1 } }
        maria_frequency_double = { name: 'Maria', frequencies: frequencies.transform_values {|v| v + 2 } }

        allow(Requester::Name).to receive(:find_name_frequencies).with('Matheus').and_return(matheus_frequency_double)
        allow(Requester::Name).to receive(:find_name_frequencies).with('João').and_return(joao_frequency_double)
        allow(Requester::Name).to receive(:find_name_frequencies).with('Maria').and_return(maria_frequency_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including(
            [
            '+==============================================================================+',
            '|                           **FREQUÊNCIA DE NOMES**                            |',
            '+==============================================================================+',
            '|     Período      ||     Matheus      ||       Joao       ||      Maria       |',
            '+==============================================================================+'
          ].join("\n"))
        ).to_stdout_from_any_process
      end

      it 'should print the contents of name frequency table' do
        arguments = ['--frequencia', '-n', 'Matheus, João, Maria']
        frequencies = {
          '1930' => 1, '1930-1940' => 2, '1940-1950' => 3, '1950-1960' => 4, '1960-1970' => 5,
          '1970-1980' => 6, '1980-1990' => 7, '1990-2000' => 8, '2000-2010' => 9
        }
        matheus_frequency_double = { name: 'Matheus', frequencies: frequencies }
        joao_frequency_double = { name: 'Joao', frequencies: frequencies.transform_values {|v| v + 1 } }
        maria_frequency_double = { name: 'Maria', frequencies: frequencies.transform_values {|v| v + 2 } }

        allow(Requester::Name).to receive(:find_name_frequencies).with('Matheus').and_return(matheus_frequency_double)
        allow(Requester::Name).to receive(:find_name_frequencies).with('João').and_return(joao_frequency_double)
        allow(Requester::Name).to receive(:find_name_frequencies).with('Maria').and_return(maria_frequency_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including(
            [
            '+==============================================================================+',
            '|       1930       ||        1         ||        2         ||        3         |',
            '+==============================================================================+',
            '|    1930-1940     ||        2         ||        3         ||        4         |',
            '+==============================================================================+',
            '|    1940-1950     ||        3         ||        4         ||        5         |',
            '+==============================================================================+',
            '|    1950-1960     ||        4         ||        5         ||        6         |',
            '+==============================================================================+',
            '|    1960-1970     ||        5         ||        6         ||        7         |',
            '+==============================================================================+',
            '|    1970-1980     ||        6         ||        7         ||        8         |',
            '+==============================================================================+',
            '|    1980-1990     ||        7         ||        8         ||        9         |',
            '+==============================================================================+',
            '|    1990-2000     ||        8         ||        9         ||        10        |',
            '+==============================================================================+',
            '|    2000-2010     ||        9         ||        10        ||        11        |',
            '+==============================================================================+'
          ].join("\n"))
        ).to_stdout_from_any_process
      end

      it 'an empty column should appear when a not found name is passed' do
        arguments = ['--frequencia', '-n', 'ZZ']
        name_double = { name: 'ZZ', frequencies: {} }
        allow(Requester::Name).to receive(:find_name_frequencies).with('ZZ').and_return(name_double)

        resp_double = double('faraday_response', status: 200, body: '[]')
        allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ZZ')
                                       .and_return(resp_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including(
            [
            '+======================================+',
            '|       **FREQUÊNCIA DE NOMES**        |',
            '+======================================+',
            '|     Período      ||        Zz        |',
            '+======================================+',
            '|       1930       ||        -         |',
            '+======================================+',
            '|    1930-1940     ||        -         |',
            '+======================================+',
            '|    1940-1950     ||        -         |',
            '+======================================+',
            '|    1950-1960     ||        -         |',
            '+======================================+',
            '|    1960-1970     ||        -         |',
            '+======================================+',
            '|    1970-1980     ||        -         |',
            '+======================================+',
            '|    1980-1990     ||        -         |',
            '+======================================+',
            '|    1990-2000     ||        -         |',
            '+======================================+',
            '|    2000-2010     ||        -         |',
            '+======================================+'
          ].join("\n"))
        ).to_stdout_from_any_process
      end

      it 'an error message should appear when at least a name is not passed' do
        arguments = ['--frequencia', '-n']

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including('Um erro ocorreu: Parece que você esqueceu de passar ao menos um nome.')
        ).to_stdout_from_any_process
      end
    end
  end

  context '.list' do
    context '.list_federative_units' do
      it 'should print the headings of federative unit listing table' do
        arguments = ['--lista', '-u']
        resp_json = File.read('./spec/support/apis/list_of_all_fus.json')
        resp_double = double('faraday_response', status: 200, body: resp_json)
        allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/?orderBy=nome')
                                      .and_return(resp_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including(
            [
              '+======================================+',
              '|       **UNIDADES FEDERATIVAS**       |',
              '+======================================+',
              '|       Nome       ||      Sigla       |',
              '+======================================+'
            ].join("\n")
          )
        ).to_stdout_from_any_process
      end

      it 'should print the contents of name frequency table' do
        arguments = ['--lista', '-u']
        resp_json = File.read('./spec/support/apis/list_of_all_fus.json')
        resp_double = double('faraday_response', status: 200, body: resp_json)
        allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/?orderBy=nome')
                                      .and_return(resp_double)

        expect { CLI::IBGENames.start(arguments) }.to output(
          a_string_including(
            [
              '+======================================+',
              '|       Nome       ||      Sigla       |',
              '+======================================+',
              '|       Acre       ||        AC        |',
              '+======================================+',
              '|     Alagoas      ||        AL        |',
              '+======================================+',
              '|      Amapá       ||        AP        |',
              '+======================================+',
              '|     Amazonas     ||        AM        |',
              '+======================================+',
              '|      Bahia       ||        BA        |',
              '+======================================+',
              '|      Ceará       ||        CE        |',
              '+======================================+',
              '| Distrito Federal ||        DF        |',
              '+======================================+',
              '|  Espírito Santo  ||        ES        |',
              '+======================================+',
              '|      Goiás       ||        GO        |',
              '+======================================+',
              '|     Maranhão     ||        MA        |',
              '+======================================+',
              '|   Mato Grosso    ||        MT        |',
              '+======================================+',
              '|Mato Grosso do Sul||        MS        |',
              '+======================================+',
              '|   Minas Gerais   ||        MG        |',
              '+======================================+',
              '|       Pará       ||        PA        |',
              '+======================================+',
              '|     Paraíba      ||        PB        |',
              '+======================================+',
              '|      Paraná      ||        PR        |',
              '+======================================+',
              '|    Pernambuco    ||        PE        |',
              '+======================================+',
              '|      Piauí       ||        PI        |',
              '+======================================+',
              '|  Rio de Janeiro  ||        RJ        |',
              '+======================================+',
              '|Rio Grande do Nort||        RN        |',
              '+======================================+',
              '|Rio Grande do Sul ||        RS        |',
              '+======================================+',
              '|     Rondônia     ||        RO        |',
              '+======================================+',
              '|     Roraima      ||        RR        |',
              '+======================================+',
              '|  Santa Catarina  ||        SC        |',
              '+======================================+',
              '|    São Paulo     ||        SP        |',
              '+======================================+',
              '|     Sergipe      ||        SE        |',
              '+======================================+',
              '|    Tocantins     ||        TO        |',
              '+======================================'
            ].join("\n")
          )
        ).to_stdout_from_any_process
      end
    end
  end
end
