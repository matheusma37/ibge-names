# frozen_string_literal: true

require_relative '../lib/requester'
require_relative '../utils/faraday_errors'

Name = Requester::Name
RJ_UF_CODE = 33
CAMPOS_CODE = 330_100_9

RSpec.describe Name do
  context '.new' do
    it 'create a new Name' do
      person_name = Name.new(name: 'Maria', frequency: 752_021, ranking: 1, sex: 'F')
      expect(person_name.name).to eql('Maria')
      expect(person_name.frequency).to eql(752_021)
      expect(person_name.ranking).to eql(1)
      expect(person_name.sex).to eql('F')
    end

    it 'raise ArgumentError' do
      expect { Name.new }.to raise_error(ArgumentError)
    end
  end

  context '.ranking_by_uf' do
    it 'should return the most frequent male names to a respective UF' do
      resp_json = File.read('./spec/support/apis/most_frequent_names_rj_m.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=33&decada=2010&sexo=M')
        .and_return(resp_double)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=33&decada=2010&sexo=F')
        .and_return(resp_double)

      names_hash = Name.ranking_by_uf(RJ_UF_CODE)
      expect(names_hash).to be_an_instance_of(Hash)
      expect(names_hash).to have_key(:male)
      expect(names_hash[:male]).to be_an_instance_of(Array)
      expect(names_hash[:male].size).to eq(20)
      expect(names_hash[:male].first.name).to eq('Joao')
      expect(names_hash[:male].last.name).to eq('Paulo')
    end

    it 'should return the most frequent female names to a respective UF' do
      resp_json = File.read('./spec/support/apis/most_frequent_names_rj_f.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=33&decada=2010&sexo=M')
        .and_return(resp_double)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=33&decada=2010&sexo=F')
        .and_return(resp_double)

      names_hash = Name.ranking_by_uf(RJ_UF_CODE)
      expect(names_hash).to be_an_instance_of(Hash)
      expect(names_hash).to have_key(:female)
      expect(names_hash[:female]).to be_an_instance_of(Array)
      expect(names_hash[:female].size).to eq(20)
      expect(names_hash[:female].first.name).to eq('Ana')
      expect(names_hash[:female].last.name).to eq('Eduarda')
    end

    it 'should return the most frequent names to a respective UF' do
      resp_m_json = File.read('./spec/support/apis/most_frequent_names_rj_m.json')
      resp_f_json = File.read('./spec/support/apis/most_frequent_names_rj_f.json')
      resp_m_double = double('faraday_response', status: 200, body: resp_m_json)
      resp_f_double = double('faraday_response', status: 200, body: resp_f_json)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=33&decada=2010&sexo=M')
        .and_return(resp_m_double)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=33&decada=2010&sexo=F')
        .and_return(resp_f_double)

      names_hash = Name.ranking_by_uf(RJ_UF_CODE)
      expect(names_hash).to be_an_instance_of(Hash)
      expect(names_hash).to have_key(:general)
      expect(names_hash[:general]).to be_an_instance_of(Array)
      expect(names_hash[:general].size).to eq(20)
      expect(names_hash[:general].first.name).to eq('Ana')
      expect(names_hash[:general].last.name).to eq('Luiz')
    end

    it 'should return a hash with empty arrays if the response status is not 200' do
      resp_double = double('faraday_response', status: 404, body: '{}')
      allow(Faraday).to receive(:get).and_return(resp_double)

      names_hash = Name.ranking_by_uf(RJ_UF_CODE)
      expect(names_hash).to eql({ female: [], male: [], general: [] })
    end

    it 'should raise an error if something goes wrong' do
      FARADAY_ERRORS.each do |error|
        allow(Faraday).to receive(:get).and_raise(error.new(''))

        expect { Name.ranking_by_uf(RJ_UF_CODE) }.to raise_error(Requester::RequestError)
      end
    end
  end

  context '.ranking_by_city' do
    it 'should return the most frequent male names to a respective City' do
      resp_json = File.read('./spec/support/apis/most_frequent_names_campos_m.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=3301009&decada=2010&sexo=M')
        .and_return(resp_double)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=3301009&decada=2010&sexo=F')
        .and_return(resp_double)

      names_hash = Name.ranking_by_city(CAMPOS_CODE)
      expect(names_hash).to be_an_instance_of(Hash)
      expect(names_hash).to have_key(:male)
      expect(names_hash[:male]).to be_an_instance_of(Array)
      expect(names_hash[:male].size).to eq(20)
      expect(names_hash[:male].first.name).to eq('Joao')
      expect(names_hash[:male].last.name).to eq('Caua')
    end

    it 'should return the most frequent female names to a respective City' do
      resp_json = File.read('./spec/support/apis/most_frequent_names_campos_f.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=3301009&decada=2010&sexo=M')
        .and_return(resp_double)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=3301009&decada=2010&sexo=F')
        .and_return(resp_double)

      names_hash = Name.ranking_by_city(CAMPOS_CODE)
      expect(names_hash).to be_an_instance_of(Hash)
      expect(names_hash).to have_key(:female)
      expect(names_hash[:female]).to be_an_instance_of(Array)
      expect(names_hash[:female].size).to eq(20)
      expect(names_hash[:female].first.name).to eq('Maria')
      expect(names_hash[:female].last.name).to eq('Brenda')
    end

    it 'should return the most frequent names to a respective City' do
      resp_m_json = File.read('./spec/support/apis/most_frequent_names_campos_m.json')
      resp_f_json = File.read('./spec/support/apis/most_frequent_names_campos_f.json')
      resp_m_double = double('faraday_response', status: 200, body: resp_m_json)
      resp_f_double = double('faraday_response', status: 200, body: resp_f_json)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=3301009&decada=2010&sexo=M')
        .and_return(resp_m_double)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/ranking?localidade=3301009&decada=2010&sexo=F')
        .and_return(resp_f_double)

      names_hash = Name.ranking_by_city(CAMPOS_CODE)
      expect(names_hash).to be_an_instance_of(Hash)
      expect(names_hash).to have_key(:general)
      expect(names_hash[:general]).to be_an_instance_of(Array)
      expect(names_hash[:general].size).to eq(20)
      expect(names_hash[:general].first.name).to eq('Maria')
      expect(names_hash[:general].last.name).to eq('Luiz')
    end

    it 'should return a hash with empty arrays if the response status is not 200' do
      resp_double = double('faraday_response', status: 404, body: '{}')
      allow(Faraday).to receive(:get).and_return(resp_double)

      names_hash = Name.ranking_by_city(CAMPOS_CODE)
      expect(names_hash).to eql({ female: [], male: [], general: [] })
    end

    it 'should raise an error if something goes wrong' do
      FARADAY_ERRORS.each do |error|
        allow(Faraday).to receive(:get).and_raise(error.new(''))

        expect { Name.ranking_by_city(CAMPOS_CODE) }.to raise_error(Requester::RequestError)
      end
    end
  end

  context '.find_name_frequencies' do
    it 'should return a name with its frequencies over the years' do
      resp_json = File.read('./spec/support/apis/frequency_over_the_years_name_joao.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get)
        .with('https://servicodados.ibge.gov.br/api/v2/censos/nomes/João')
        .and_return(resp_double)

      name = Name.find_name_frequencies('João')
      expect(name).to be_an_instance_of(Hash)
      expect(name).to have_key(:name)
      expect(name[:name]).to eq('Joao')
      expect(name).to have_key(:frequencies)
      expect(name[:frequencies]).to be_an_instance_of(Hash)
      expect(name[:frequencies]['1930']).to eq(60_155)
      expect(name[:frequencies]['2000-2010']).to eq(794_118)
    end

    it 'should return a hash with empty arrays if the response status is not 200' do
      resp_double = double('faraday_response', status: 404, body: '{}')
      allow(Faraday).to receive(:get).and_return(resp_double)

      name = Name.find_name_frequencies('')
      expect(name).to eql({ name: '', frequencies: {} })
    end

    it 'should raise an error if something goes wrong' do
      FARADAY_ERRORS.each do |error|
        allow(Faraday).to receive(:get).and_raise(error.new(''))

        expect { Name.find_name_frequencies('Joao') }.to raise_error(Requester::RequestError)
      end
    end
  end
end
