# frozen_string_literal: true

require_relative '../lib/cli'

RSpec.describe CLI::OptionParser do
  context 'should parse the arguments' do
    it 'with -h or --help and show the help options' do
      expect { system './nomes_ibge -h' }.to output(
        a_string_including('Uso: ./nomes_ibge [opções]')
      ).to_stdout_from_any_process
      expect { system './nomes_ibge --help' }.to output(
        a_string_including('Uso: ./nomes_ibge [opções]')
      ).to_stdout_from_any_process
    end

    it 'with -v or --version and show the help options' do
      expect { system './nomes_ibge -v' }.to output(
        a_string_including(CLI::OptionParser::VERSION)
      ).to_stdout_from_any_process
      expect { system './nomes_ibge --version' }.to output(
        a_string_including(CLI::OptionParser::VERSION)
      ).to_stdout_from_any_process
    end

    it 'and raise an exception if pass an invalid flag' do
      parser = CLI::OptionParser.new

      expect { parser.parse(%w[-z]) }.to raise_error(ArgumentError)
      expect { parser.parse(%w[--zzz]) }.to raise_error(ArgumentError)
    end
  end

  context '.parse' do
    context 'should parse the arguments for the ranking of names' do
      it 'with -r/--ranking' do
        parser = CLI::OptionParser.new

        short_result = parser.parse(%w[-r])
        long_result = parser.parse(%w[--ranking])

        expect(short_result[:option]).to eql(:ranking)
        expect(long_result[:option]).to eql(:ranking)
      end

      context 'with Federative Unit' do
        it 'specified' do
          parser = CLI::OptionParser.new

          short_result = parser.parse(%w[-r -u RJ])
          long_result = parser.parse(%w[--ranking --unidade-federativa RJ])

          expect(short_result[:arguments][:federative_unit]).to eql('RJ')
          expect(long_result[:arguments][:federative_unit]).to eql('RJ')
        end

        it 'and raise an exception if do not pass --ranking/-r flag' do
          parser = CLI::OptionParser.new

          expect { parser.parse(%w[-u RJ]) }.to raise_error(ArgumentError)
          expect { parser.parse(%w[--unidade-federativa RJ]) }.to raise_error(ArgumentError)
        end

        it 'and raise an exception if do not pass an UF' do
          parser = CLI::OptionParser.new

          expect { parser.parse(%w[-r -u]) }.to raise_error(ArgumentError)
          expect { parser.parse(%w[--ranking --unidade-federativa]) }.to raise_error(ArgumentError)
        end
      end

      context 'with City' do
        it 'specified' do
          parser = CLI::OptionParser.new

          short_result = parser.parse(['-r', '-c', 'Campos dos Goytacazes'])
          long_result = parser.parse(['--ranking', '--cidade', 'Campos dos Goytacazes'])

          expect(short_result[:arguments][:city]).to eql('Campos dos Goytacazes')
          expect(long_result[:arguments][:city]).to eql('Campos dos Goytacazes')
        end

        it 'and raise an exception if do not pass --ranking/-r flag' do
          parser = CLI::OptionParser.new

          expect { parser.parse(%w[-c "Campos dos Goytacazes"]) }.to raise_error(ArgumentError)
          expect { parser.parse(%w[--cidade "Campos dos Goytacazes"]) }.to raise_error(ArgumentError)
        end

        it 'and raise an exception if do not pass a City name' do
          parser = CLI::OptionParser.new

          expect { parser.parse(%w[-r -c]) }.to raise_error(ArgumentError)
          expect { parser.parse(%w[--ranking --cidade]) }.to raise_error(ArgumentError)
        end
      end
    end

    context 'should parse the arguments for the frequencies of' do
      it 'not specified' do
        parser = CLI::OptionParser.new

        short_result = parser.parse(%w[-f])
        long_result = parser.parse(%w[--frequencia])

        expect(short_result[:option]).to eql(:frequency)
        expect(long_result[:option]).to eql(:frequency)
      end

      context 'Names' do
        it 'one specified' do
          parser = CLI::OptionParser.new

          short_result = parser.parse(['-f', '-n', 'Matheus'])
          long_result = parser.parse(['--frequencia', '--nomes', 'Matheus'])

          expect(short_result[:arguments][:names]).to eql(['Matheus'])
          expect(long_result[:arguments][:names]).to eql(['Matheus'])
        end

        it 'more than one specified' do
          parser = CLI::OptionParser.new

          short_result = parser.parse(['-f', '-n', 'Matheus, João, Maria'])
          long_result = parser.parse(['--frequencia', '--nomes', 'Matheus, João, Maria'])

          expect(short_result[:arguments][:names]).to eql(['Matheus', 'João', 'Maria'])
          expect(long_result[:arguments][:names]).to eql(['Matheus', 'João', 'Maria'])
        end

        it 'and raise an exception if do not pass --frequencia/-r flag' do
          parser = CLI::OptionParser.new

          expect { parser.parse(%w[-n Matheus]) }.to raise_error(ArgumentError)
          expect { parser.parse(%w[--nomes Matheus]) }.to raise_error(ArgumentError)
        end

        it 'and raise an exception if do not pass names' do
          parser = CLI::OptionParser.new

          expect { parser.parse(%w[-f -n]) }.to raise_error(ArgumentError)
          expect { parser.parse(%w[--frequencia --nomes]) }.to raise_error(ArgumentError)
        end
      end
    end

    context 'should parse the arguments for the listing of' do
      it 'not specified' do
        parser = CLI::OptionParser.new

        short_result = parser.parse(%w[-l])
        long_result = parser.parse(%w[--lista])

        expect(short_result[:option]).to eql(:list)
        expect(long_result[:option]).to eql(:list)
      end

      context 'FederativeUnit' do
        it 'all' do
          parser = CLI::OptionParser.new

          short_result = parser.parse(['-l', '-u'])
          long_result = parser.parse(['--lista', '--unidade-federativa'])

          expect(short_result[:arguments][:federative_unit]).to eql(true)
          expect(long_result[:arguments][:federative_unit]).to eql(true)
        end

        it 'and raise an exception if do not pass --unidade-federativa/-u flag' do
          parser = CLI::OptionParser.new

          expect { parser.parse(['-u']) }.to raise_error(ArgumentError)
          expect { parser.parse(['--unidade-federativa']) }.to raise_error(ArgumentError)
        end

        it 'and raise an exception if pass an invalid option' do
          parser = CLI::OptionParser.new

          expect { parser.parse(['-l', '-z']) }.to raise_error(ArgumentError)
          expect { parser.parse(['-l', '--zz']) }.to raise_error(ArgumentError)
        end
      end
    end
  end
end
