# frozen_string_literal: true

require_relative '../lib/table_formatter'

RSpec.describe TableFormatter do
  context '.new' do
    it 'create a new TableFormatter' do
      tf = TableFormatter.new(title: 'Geral', rows: [])
      expect(tf.title).to eql('Geral')
      expect(tf.rows).to eql([])
    end

    it 'raise ArgumentError' do
      expect { TableFormatter.new }.to raise_error(ArgumentError)
    end
  end

  context '.format_table' do
    it 'should format a table' do
      rows = [
        [1, 'Ana', 82_302],
        [2, 'Maria', 78_499],
        [3, 'Joao', 69_336],
        [4, 'Gabriel', 52_945],
        [5, 'Lucas', 45_410]
      ]
      table = [
        '+==========================================================+',
        '|                        **GERAL**                         |',
        '+==========================================================+',
        '|     Ranking      ||       Nome       ||    Frequência    |',
        '+==========================================================+',
        '|        1         ||       Ana        ||      82302       |',
        '+==========================================================+',
        '|        2         ||      Maria       ||      78499       |',
        '+==========================================================+',
        '|        3         ||       Joao       ||      69336       |',
        '+==========================================================+',
        '|        4         ||     Gabriel      ||      52945       |',
        '+==========================================================+',
        '|        5         ||      Lucas       ||      45410       |',
        '+==========================================================+'
      ]
      tf = TableFormatter.new(title: 'Geral', rows: rows)
      formated_table = tf.format_table
      expect(formated_table).to eq(table.join("\n"))
    end

    it 'should return just the header and description for empty values' do
      rows = []
      table = [
        '+==========================================================+',
        '|                        **GERAL**                         |',
        '+==========================================================+',
        '|     Ranking      ||       Nome       ||    Frequência    |',
        '+==========================================================+'
      ]
      tf = TableFormatter.new(title: 'Geral', rows: rows)
      formated_table = tf.format_table
      expect(formated_table).to eq(table.join("\n"))
    end
  end
end
