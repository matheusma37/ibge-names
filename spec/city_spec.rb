# frozen_string_literal: true

require_relative '../lib/requester'
require_relative '../utils/faraday_errors'

City = Requester::City

RSpec.describe City do
  context '.new' do
    it 'create a new City' do
      city = City.new(id: 330_100_9, name: 'Campos dos Goytacazes', federative_unit_id: 33)
      expect(city.id).to eql(330_100_9)
      expect(city.name).to eql('Campos dos Goytacazes')
      expect(city.federative_unit_id).to eql(33)
    end

    it 'raise ArgumentError' do
      expect { City.new }.to raise_error(ArgumentError)
    end
  end

  context '.ranking_by_id' do
    it 'should return Campos dos Goytacazes city code to a id = 3301009' do
      resp_json = File.read('./spec/support/apis/campos_dos_goytacazes.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/municipios/3301009')
                                     .and_return(resp_double)

      rj = City.ranking_by_id('3301009')
      expect(rj.id).to eql(330_100_9)
      expect(rj.name).to eql('Campos dos Goytacazes')
      expect(rj.federative_unit_id).to eql(33)
    end

    it 'should return Campos dos Goytacazes city code to a id = "Campos dos Goytacazes"' do
      resp_json = File.read('./spec/support/apis/campos_dos_goytacazes.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/municipios/Campos-dos-Goytacazes')
                                     .and_return(resp_double)

      rj = City.ranking_by_id('Campos dos Goytacazes')
      expect(rj.id).to eql(330_100_9)
      expect(rj.name).to eql('Campos dos Goytacazes')
      expect(rj.federative_unit_id).to eql(33)
    end

    it 'should raise an error if something goes wrong' do
      FARADAY_ERRORS.each do |error|
        allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/municipios/3301009')
                                       .and_raise(error.new(''))

        expect { City.ranking_by_id('3301009') }.to raise_error(Requester::RequestError)
      end
    end

    it 'should raise an error if the City is invalid' do
      resp_double = double('faraday_response', status: 200, body: '[]')
      allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/municipios/Z-City')
                                     .and_return(resp_double)

      expect { City.ranking_by_id('Z-City') }.to raise_error(ArgumentError)
    end
  end
end
