# frozen_string_literal: true

require_relative '../lib/requester'
require_relative '../utils/faraday_errors'

FederativeUnit = Requester::FederativeUnit

FEDERATIVE_UNITS = %w[
  AC AL AM AP BA CE DF ES GO MA MG MS MT
  PA PB PE PI PR RJ RN RO RR RS SC SP SE TO
].freeze

RSpec.describe FederativeUnit do
  context '.new' do
    it 'create a new FU' do
      fu = FederativeUnit.new(id: 33, initials: 'RJ', name: 'Rio de Janeiro')
      expect(fu.id).to eql(33)
      expect(fu.initials).to eql('RJ')
    end

    it 'raise ArgumentError' do
      expect { FederativeUnit.new }.to raise_error(ArgumentError)
    end
  end

  context '.all' do
    it 'should return an array with all FUs' do
      resp_json = File.read('./spec/support/apis/list_of_all_fus.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/?orderBy=nome')
                                     .and_return(resp_double)

      fu_list = FederativeUnit.all
      fu_list.each do |fu|
        expect(FEDERATIVE_UNITS).to include(fu.initials)
      end
    end

    it 'should have AC as the first element' do
      resp_json = File.read('./spec/support/apis/list_of_all_fus.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/?orderBy=nome')
                                     .and_return(resp_double)

      fu_list = FederativeUnit.all
      expect(fu_list.first.initials).to eql(FEDERATIVE_UNITS.first)
    end

    it 'should have TO as the last element' do
      resp_json = File.read('./spec/support/apis/list_of_all_fus.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/?orderBy=nome')
                                     .and_return(resp_double)

      fu_list = FederativeUnit.all
      expect(fu_list.last.initials).to eql(FEDERATIVE_UNITS.last)
    end

    it 'should raise an error if something goes wrong' do
      FARADAY_ERRORS.each do |error|
        allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/?orderBy=nome')
                                       .and_raise(error.new(''))

        expect { FederativeUnit.all }.to raise_error(Requester::RequestError)
      end
    end
  end

  context '.ranking_by_id' do
    it 'should return Rio de Janeiro FU code to a id = 33' do
      resp_json = File.read('./spec/support/apis/rj_federative_unit.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/33')
                                     .and_return(resp_double)

      rj = FederativeUnit.ranking_by_id('33')
      expect(rj.id).to eql(33)
      expect(rj.initials).to eql('RJ')
      expect(rj.name).to eql('Rio de Janeiro')
    end

    it 'should return Rio de Janeiro FU code to a id = RJ' do
      resp_json = File.read('./spec/support/apis/rj_federative_unit.json')
      resp_double = double('faraday_response', status: 200, body: resp_json)
      allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/RJ')
                                     .and_return(resp_double)

      rj = FederativeUnit.ranking_by_id('RJ')
      expect(rj.id).to eql(33)
      expect(rj.initials).to eql('RJ')
      expect(rj.name).to eql('Rio de Janeiro')
    end

    it 'should raise an error if something goes wrong' do
      FARADAY_ERRORS.each do |error|
        allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/33')
                                       .and_raise(error.new(''))

        expect { FederativeUnit.ranking_by_id('33') }.to raise_error(Requester::RequestError)
      end
    end

    it 'should raise an error if the Federative Unit is invalid' do
      resp_double = double('faraday_response', status: 200, body: '[]')
      allow(Faraday).to receive(:get).with('https://servicodados.ibge.gov.br/api/v1/localidades/estados/ZZ')
                                     .and_return(resp_double)

      expect { FederativeUnit.ranking_by_id('ZZ') }.to raise_error(ArgumentError)
    end
  end
end
