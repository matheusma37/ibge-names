# frozen_string_literal: true

require 'faraday'

FARADAY_ERRORS = [
  Faraday::ClientError, Faraday::BadRequestError, Faraday::UnauthorizedError,
  Faraday::ForbiddenError, Faraday::ResourceNotFound, Faraday::ProxyAuthError,
  Faraday::ConflictError, Faraday::UnprocessableEntityError, Faraday::ServerError,
  Faraday::TimeoutError, Faraday::NilStatusError, Faraday::ConnectionFailed,
  Faraday::SSLError, Faraday::ParsingError, Faraday::RetriableResponse
].freeze
