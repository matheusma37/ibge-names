# Nomes IBGE
### Tabela de Conteúdos
[[_TOC_]]
## Descrição
<p align="justify"> Programa de linha de comando para listagem de nomes mais comuns em localidades do Brasil.</p>

[![Badge](https://img.shields.io/static/v1?label=ruby&message=2.7.2&color=red&style=for-the-badge&logo=ruby)](#)
[![License](https://img.shields.io/badge/license-mit-green.svg?style=for-the-badge)](LICENSE)
> Status do Projeto: Em desenvolvimento :warning: :construction_worker:

### Principais Funcionalidades
- [X] Ranking dos nomes mais populares de um Estado
- [X] Ranking dos nomes mais populares de uma Cidade
- [X] Histórico de uso de um nome ao longo dos anos

## Como Rodar a Aplicação
### Pré-Requisitos
+ Ruby - 2.7.2
+ Bundler - 2.1.4

### Instalação, Execução e Teste
Clone o repositório do Github
```
$ git clone https://gitlab.com/matheusma37/ibge-names.git
```
ou
```
$ git clone git@gitlab.com:matheusma37/ibge-names.git
```
Entre no repositório
```
$ cd ibge-names/
```
Execute o arquivo de configuração
```
$ bin/setup
```
Teste o projeto
```
$ bundle exec rspec
```

## Usando a Aplicação
Para obter o ranking de nomes mais comuns em um Estado, entre com o comando:
```
$ ./nomes_ibge --ranking --unidade-federativa [UF]
```
> O ranking é referente a década de 2010, a até então mais recente década disponível na API
>> Exemplo de uso: `./nomes_ibge --ranking --unidade-federativa RJ`

Para obter o ranking de nomes mais comuns em uma Cidade, entre com o comando:
```
$ ./nomes_ibge --ranking --cidade [CIDADE]
```
> O ranking é referente a década de 2010, a até então mais recente década disponível na API
>> Exemplo de uso: `./nomes_ibge --ranking --cidade "São Luís"`

Para obter a frequência de ocorrência de um ou mais nomes ao decorrer dos anos, entre com o comando:
```
$ ./nomes_ibge --frequencia --nome NOME1,NOME2,NOME3
```
> A frequência começa a partir da década de 1930 e termina em 2010, porém nem todos os nomes possuem informações em todas as décadas dessa faixa. Décadas que não possuem dados de um nome são preenchidas com um "-". Os nomes devem estar colados a vírgula que vem antes deles para serem reconhecidos.
>> Exemplo de uso: `./nomes_ibge --frequencia --nomes Matheus,João,Maria`

Para obter uma tabela listando informações de determinado assunto, entre com o comando:
```
$ ./nomes_ibge --lista [OPÇÕES]
```
> Opções válidas para o comando `--lista` são: `-u` e `--unidade-federativa`, ambas servem para listar todas as Unidades Federativas disponíveis.
>> Exemplo de uso: `./nomes_ibge --lista --unidade-federativa`

Para obter mais informações sobre os comandos, use:
```
$ ./nomes_ibge --help
```

Para obter a versão da aplicação, use:
```
$ ./nomes_ibge --version
```

## Questões do Projeto
### Elaboração das Tabelas
A elaboração da tabela "__*GERAL*__" é feita com base nas outras duas tabelas: "__*FEMININO*__" e "__*MASCULINO*__". Essa escolha foi tomada com base em uma inconsistência, no retorno, percebida durante o desenvolvimento. Ao fazer uma requisição sem passar o argumento de sexo, os nomes retornados possuíam um dado valor de frequência. Já quando o argumento de sexo era passado, o mesmo nome retornava com um outro valor. Sendo assim, foi escolhida essa abordagem para garantir que as informações presentes nas tabelas mantivessem-se consistentes entre si.

### Década Padrão para os Rankings
A funcionalidade de ranking dos nomes tem sua década de busca fixada em 2010. Essa escolha foi feita devido a não especificação na API da década retornada por padrão. Sendo assim, foi feita a escolha pela informação mais recente disponível na mesma.

## Linguagens, Dependências e Libs Usadas
+ [Accentless](https://github.com/lucasas/accentless)
+ [Byebug](https://github.com/deivid-rodriguez/byebug)
+ [Faraday](https://github.com/lostisland/faraday)
+ [RSpec](https://github.com/rspec/rspec)
+ [Rubocop](https://github.com/rubocop/rubocop)
+ [Simplecov](https://github.com/simplecov-ruby/simplecov)

## Fontes de Dados
+ [API Localidades](https://servicodados.ibge.gov.br/api/docs/localidades)
+ [API Nomes](https://servicodados.ibge.gov.br/api/docs/nomes?versao=2)