# frozen_string_literal: true

require 'optparse'
require_relative 'table_formatter'
require_relative 'requester'

OptParser = OptionParser

# Namespace containing the classes responsible for connecting the user with the functionalities
module CLI
  # Class responsible for parse passed arguments as options for the program
  class OptionParser
    VERSION = '0.1.2'
    # Class responsible for defining options for the Parser
    class ScriptOptions
      attr_reader :arguments

      def initialize
        @arguments = {}
      end

      def define_options(parser)
        options_header(parser)

        list(parser)
        ranking(parser)
        ranking_federative_unit_names(parser)
        ranking_city_names(parser)
        frequency(parser)
        frequency_names(parser)

        options_footer(parser)
      end

      private

      def ranking(parser)
        parser.on('-r', '--ranking', 'Mostra um ranking de acordo com a opção que vier na sequência') do
          raise(ArgumentError, 'Opção inválida!') if @arguments[:option] == :frequency
          raise(ArgumentError, 'Opção inválida!') if @arguments[:option] == :list

          @arguments[:option] = :ranking
        end
      end

      def ranking_federative_unit_names(parser)
        parser.on('-u', '--unidade-federativa [UF]',
                  'Mostra o ranking dos 20 nomes mais populares em determinada UF',
                  "\tExemplo de uso: ./nomes_ibge -r -u RJ") do |fu|
          raise(ArgumentError, 'Opção inválida!') unless %i[ranking list].include?(@arguments[:option])
          raise(ArgumentError, 'Parece que você esqueceu de passar uma UF.') if fu.nil? && @arguments[:option] != :list

          @arguments[:arguments] = { federative_unit: fu } if @arguments[:option] == :ranking
          @arguments[:arguments] = { federative_unit: true } if @arguments[:option] == :list
        end
      end

      def ranking_city_names(parser)
        parser.on('-c', '--cidade [CIDADE]',
                  'Mostra o ranking dos 20 nomes mais populares em determinada Cidade',
                  "\tExemplo de uso: ./nomes_ibge -r -c Florianópolis",
                  'Cidades com nomes compostos devem ter seu nome passado entre aspas simples ou duplas',
                  "\tExemplo de uso: ./nomes_ibge -r -c \"Campos dos Goytacazes\"") do |city|
          raise(ArgumentError, 'Opção inválida!') unless @arguments[:option] == :ranking
          raise(ArgumentError, 'Parece que você esqueceu de passar o nome de uma cidade.') if city.nil?

          @arguments[:arguments] = { city: city }
        end
      end

      def frequency(parser)
        parser.on('-f', '--frequencia', 'Mostra a frequência de algo de acordo com a opção que vier na sequência') do
          raise(ArgumentError, 'Opção inválida!') if @arguments[:option] == :ranking
          raise(ArgumentError, 'Opção inválida!') if @arguments[:option] == :list

          @arguments[:option] = :frequency
        end
      end

      def frequency_names(parser)
        parser.on('-n', '--nomes nome1,nome2,nome3', Array,
                  'Mostra a frequência com que um ou mais nomes foram usados entre 1930 e 2010',
                  "\tExemplo de uso: ./nomes_ibge -f -n Matheus,João,Maria") do |names|
          raise(ArgumentError, 'Opção inválida!') unless @arguments[:option] == :frequency

          @arguments[:arguments] = { names: names.map(&:strip) }
        end
      end

      def list(parser)
        parser.on('-l', '--lista',
                  'Mostra uma lista de acordo com a opção que vier na sequência',
                  "\tExemplo de uso: ./nomes_ibge -l -u") do
          raise(ArgumentError, 'Opção inválida!') if @arguments[:option] == :frequency
          raise(ArgumentError, 'Opção inválida!') if @arguments[:option] == :ranking

          @arguments[:option] = :list
        end
      end

      def options_header(parser)
        parser.banner = 'Uso: ./nomes_ibge [opções]'
        parser.separator ''
        parser.separator 'Opções específicas:'
      end

      def options_footer(parser)
        parser.separator ''
        parser.separator 'Opções comuns:'
        parser.on_tail('-h', '--help', 'Exibe essa mensagem') do
          puts parser
          exit
        end
        parser.on_tail('-v', '--version', 'Exibe a versão') do
          puts VERSION
          exit
        end
      end
    end

    def parse(args)
      script_options = ScriptOptions.new

      OptParser.new do |parser|
        script_options.define_options(parser)
        parser.parse!(args)
      end

      script_options.arguments
    rescue OptParser::InvalidOption => e
      raise ArgumentError, "Opção inválida \"#{e.message.split[-1]}\""
    rescue OptParser::MissingArgument
      raise(ArgumentError, 'Parece que você esqueceu de passar ao menos um nome.')
    end
  end

  # Main class, responsible for connecting all things
  class IBGENames
    TITLES = {
      general: 'Geral',
      female: 'Feminino',
      male: 'Masculino'
    }.freeze

    def initialize
      raise StandardError, 'Não é permitido instanciar essa classe.'
    end

    class << self
      def start(args)
        arguments = OptionParser.new.parse(args)
        ranking(arguments) if arguments[:option] == :ranking
        frequency(arguments) if arguments[:option] == :frequency
        list(arguments) if arguments[:option] == :list
      rescue ArgumentError => e
        puts "Um erro ocorreu: #{e.message}"
      end

      private

      def ranking(arguments)
        federative_unit_names(arguments) if arguments[:arguments][:federative_unit]
        city_names(arguments) if arguments[:arguments][:city]
      end

      def frequency(arguments)
        names_frequencies(arguments[:arguments][:names]) if arguments[:arguments][:names]
      end

      def list(arguments)
        list_federative_units if arguments[:arguments][:federative_unit]
      end

      def federative_unit_names(arguments)
        federative_unit = Requester::FederativeUnit.ranking_by_id(arguments[:arguments][:federative_unit])
        result = Requester::Name.ranking_by_uf(federative_unit.id)
        tables = build_ranking_tables(result)
        print_tables(federative_unit.name, tables)
      end

      def city_names(arguments)
        city = Requester::City.ranking_by_id(arguments[:arguments][:city])
        result = Requester::Name.ranking_by_city(city.id)
        tables = build_ranking_tables(result)
        print_tables(city.name, tables)
      end

      def names_frequencies(names)
        names_hash = names.map do |name|
          Requester::Name.find_name_frequencies(name)
        end
        frequency_table = build_frequency_table(names_hash)
        puts frequency_table
      end

      def list_federative_units
        federative_units = Requester::FederativeUnit.all
        headings = %w[nome sigla]
        rows = federative_units.map { |fu| [fu.name, fu.initials] }
        puts TableFormatter.new(title: 'Unidades Federativas', rows: rows, headings: headings).format_table
      end

      def print_tables(title, tables)
        puts '=' * 80
        puts " Ranking de Nomes #{title} ".center(80, '=')
        tables.each { |table| puts "#{table}\n\n" }
      end

      def build_ranking_tables(result)
        result.map do |key, rows|
          rows = rows.map { |name| [name.ranking, name.name, name.frequency] }
          TableFormatter.new(title: TITLES[key], rows: rows).format_table
        end.reverse
      end

      def build_frequency_table(names_hash)
        headings = ['período', *names_hash.map { |name| name[:name] }]
        names_frequencies = names_hash.map do |name|
          Requester::Name::PERIODS.map { |period| name[:frequencies].fetch(period, '-') }
        end
        rows = Requester::Name::PERIODS.zip(*names_frequencies)
        TableFormatter.new(title: 'Frequência de Nomes', rows: rows, headings: headings).format_table
      end
    end
  end
end
