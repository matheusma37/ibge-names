# frozen_string_literal: true

# Class responsible for formatting presentation tables
class TableFormatter
  BASE_CELL_SIZE = 20
  PARALLEL_SIDE_COUNT = 2

  attr_reader :title, :rows, :headings

  def initialize(title:, rows:, headings: %w[Ranking Nome Frequência])
    @title = title
    @rows = rows
    @headings = headings
  end

  def format_table
    (header + description + line + content).strip
  end

  private

  def line
    "+#{'=' * table_width}+\n"
  end

  def cell(word, size)
    word = word.to_s
    word = word[0..(size - 1)]
    "|#{word.center(size)}|"
  end

  def content_line(row)
    row.reduce('') { |result, elem| "#{result}#{cell(elem, cell_size)}" }
  end

  def header
    "#{line}#{cell("**#{title.upcase}**", table_width)}\n#{line}"
  end

  def description
    "#{content_line(headings.map(&:capitalize))}\n"
  end

  def content
    rows.reduce('') do |result, row|
      "#{result}#{content_line(row)}\n#{line}"
    end
  end

  def table_width
    headings.size * BASE_CELL_SIZE - PARALLEL_SIDE_COUNT
  end

  def cell_size
    table_width / headings.size - 1
  end
end
