# frozen_string_literal: true

require 'json'
require 'accentless'
require_relative '../utils/faraday_errors'

# Namespace containing classes responsible for requesting API values
module Requester
  # Module containing the methods common to the requestable classes
  module Requestable
    private

    def join_path(*path_chunks)
      self::BASE_URL + path_chunks.join
    end

    def send_request(url_path)
      response = Faraday.get(url_path)
      response_json = JSON.parse(response.body, symbolize_names: true)
      yield response_json, response
    rescue *FARADAY_ERRORS
      raise(RequestError, 'Houve um erro durante a requisição. Verifique sua conexão e tente novamente.')
    end
  end

  # Class responsible for requesting the Federative Unit API
  class FederativeUnit
    extend Requestable

    BASE_URL = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados/'

    attr_reader :id, :initials, :name

    def initialize(id:, initials:, name:)
      @id = id
      @initials = initials
      @name = name
    end

    class << self
      def all
        send_request(join_path('?orderBy=nome')) do |response_json, _|
          response_json.map { |element| new(id: element[:id], initials: element[:sigla], name: element[:nome]) }
        end
      end

      def ranking_by_id(id)
        id = id.gsub(/('|")/, '').gsub(/\s/, '')
        send_request(join_path(id)) do |response_json, _|
          raise(ArgumentError, 'Unidade Federativa inválida.') if response_json == []

          new(id: response_json[:id], initials: response_json[:sigla], name: response_json[:nome])
        end
      end
    end
  end

  # Class responsible for requesting the City API
  class City
    extend Requestable
    using Accentless

    BASE_URL = 'https://servicodados.ibge.gov.br/api/v1/localidades/municipios/'

    attr_reader :id, :name, :federative_unit_id

    def initialize(id:, name:, federative_unit_id:)
      @id = id
      @name = name
      @federative_unit_id = federative_unit_id
    end

    class << self
      def ranking_by_id(id)
        id = id.accentless.gsub(/('|")/, '').split.join('-')
        send_request(join_path(id)) do |response_json, _|
          raise(ArgumentError, 'Cidade inválida.') if response_json == []

          new(
            id: response_json[:id],
            name: response_json[:nome],
            federative_unit_id: response_json[:microrregiao][:mesorregiao][:UF][:id]
          )
        end
      end
    end
  end

  # Class responsible for requesting the Name API
  class Name
    extend Requestable
    using Accentless

    BASE_URL = 'https://servicodados.ibge.gov.br/api/v2/censos/nomes/'
    SEX = { male: 'M', female: 'F' }.freeze
    PERIODS = %w[1930 1930-1940 1940-1950 1950-1960 1960-1970 1970-1980 1980-1990 1990-2000 2000-2010].freeze

    attr_reader :name, :frequency, :sex, :ranking

    def initialize(name:, frequency:, sex:, ranking:)
      @name = name
      @frequency = frequency
      @sex = sex
      @ranking = ranking
    end

    class << self
      def ranking_by_uf(federative_unit)
        build_ranking_response(federative_unit)
      rescue NoMethodError
        { male: [], female: [], general: [] }
      end

      def ranking_by_city(city)
        city = city.to_s.accentless.gsub(/('|")/, '').split.join('-')
        build_ranking_response(city)
      rescue NoMethodError
        { male: [], female: [], general: [] }
      end

      def find_name_frequencies(name)
        name = name.accentless.gsub(/('|")/, '')
        send_request(join_path(name.gsub(' ', '%20'))) do |response_json, _|
          { name: response_json[0][:nome].capitalize,
            frequencies: response_json[0][:res].map do |frequency|
              [frequency[:periodo].gsub(/[^\d]/, ',' => '-'), frequency[:frequencia]]
            end.to_h }
        end
      rescue NoMethodError
        { name: name, frequencies: {} }
      end

      private

      def ranking_by(relative_url, decade: '2010', sex: 'M')
        send_request(join_path(relative_url, "&decada=#{decade}", "&sexo=#{sex}")) do |response_json, _|
          response_json[0][:res].map do |element|
            new(
              name: element[:nome].capitalize,
              frequency: element[:frequencia],
              ranking: element[:ranking],
              sex: sex
            )
          end
        end
      end

      def select_general_ranking(values)
        general = values.flatten.sort { |a, b| b.frequency <=> a.frequency }.take(20)
        general.map.with_index(1) do |element, index|
          new(
            name: element.name,
            frequency: element.frequency,
            ranking: index,
            sex: element.sex
          )
        end
      end

      def build_ranking_response(id)
        response_hash = {}
        response_hash[:male] = ranking_by("ranking?localidade=#{id}")
        response_hash[:female] = ranking_by("ranking?localidade=#{id}", sex: SEX[:female])
        response_hash[:general] = select_general_ranking(response_hash.values)
        response_hash
      end
    end
  end

  class RequestError < StandardError; end
end
